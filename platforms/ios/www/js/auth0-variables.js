  /*
    File Name   :      auth0-variables.js
    Project     :      DameDashStudios IOS App
    Copyright (c)      www.damedashstudios.com
    author      :      Prasanna 
    license     :   
    version     :      0.0.1 
    Created on  :      August ‎22, ‎2016
    Last modified on:  August ‎22, ‎2016 
    Description :      This file contains Auth0 App ID details                         . 
    Organisation:      Peafowl inc.  
    */
var AUTH0_CLIENT_ID='xZv3VKuoGDkm7BXCv4V0efAj0C8OzHMB'; 
var AUTH0_DOMAIN='damedashstudios.auth0.com'; 
var AUTH0_CALLBACK_URL=location.href;